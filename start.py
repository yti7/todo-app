from os import system
from colored import fg, attr

green = fg('green')
yellow = fg('yellow')
attri = attr(0)


try:
	print(f'[+] Running YARN START.')
	system('yarn start')
except:
	print(f'[-] Development server stopped')
	input('')
