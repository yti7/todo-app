import React from "react";
import './ComponentCss/Sidebar.css'
import AccountProfile from "./BasicComponents/AccountProfile";
import { Link } from "react-router-dom";

const Sidebar = () => {
  return (
    <>
    <div id="sidebar">
    <div className="hr"></div>
    <AccountProfile />
    <div className="hr"></div>

    <div className="sidebar-menus-list gd">
      <Link to='/myday' className="sidebar-menus cur-p">
        <i className="uil uil-sun menu-icons my-day-icon"></i> 
        <p className="my-day-p">My Day</p>
      </Link>
      <Link to='/planned' className="sidebar-menus cur-p">
        <i className="uil uil-schedule menu-icons planned-icon"></i> 
        <p className="planned-p">Planned</p>
      </Link>
      <Link to='/all' className="sidebar-menus cur-p">
        <i className="uil uil-list-ul menu-icons all-icon"></i>
        <p className="all-p">All</p>
      </Link>
    </div>

    <div className="hr"></div>
    </div>
    </>
  );
};

export default Sidebar;
