import MainApp from "./MainApp"

const App = ()=>{
  return (
    <div className="App">
      <MainApp />
    </div>
  );
}

export default App;
