import React from 'react'
import './BasicComponentsCss/AccountProfile.css'

const AccountProfile = () => {
  return (
    <>
      <div className="account-profile gd">
        <div className="account-profile--icon" id="a-p--icon">
          <i className="uil uil-user-circle profile-user-icon"></i>
        </div>
        <p className="account-profile--name">Yash</p>
        <p className="account-profile--email">yash@titanium.ml</p>
      </div>
    </>
  )
}

export default AccountProfile
