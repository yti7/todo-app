import './ComponentCss/Header.css'

const Header = ({ section }) => {
  
  const closeSidebar = () => {
    const sidebar = document.querySelector('#sidebar')
    const acProfileIcon = document.querySelector('#a-p--icon')
    let button = document.querySelector('.slide-button')
    let width = sidebar.style.width
    if(width == '20%' || width == ''){
      sidebar.style.width = '56px'
      acProfileIcon.style.transform = 'translateX(-15px)'
      button.style.transform = 'rotateY(0deg)'
    } else {
      sidebar.style.width = '20%'
      acProfileIcon.style.transform = 'translateX(0px)'
      button.style.transform = 'rotateY(180deg)'
    }
    console.log(sidebar.style.width)
  }
  
  return (
    <div className="header gd">
      <button className="slide-button cur-p" onClick={() => closeSidebar()}>
        <i class="uil uil-angle-double-right"></i>
      </button>
      <div className="header-data">
        <h1 className="heading">{section}</h1>
      </div>
    </div>
  )
}

export default Header
